﻿Imports System.IO
Imports System.Windows.Forms
Imports Microsoft.Office.Interop.Outlook

Public Class ExportManager
    
    Private ReadOnly ImageExtensions as String() = {".PNG", ".JPG", ".JPEG", ".BMP", ".GIF", ".TIFF"}
    Private ReadOnly Application As Microsoft.Office.Interop.Outlook.Application

    public Sub New (app As Outlook.Application)
        Me.Application = app
    End Sub

    ''' <summary>
    ''' Try to save the images to a folder
    ''' </summary>
    public sub TrySaveImages()
        '' If there is no selection, just exit.
        If (Me.Application.ActiveExplorer.Selection.Count <= 0) Then
            ShowMessage("No Emails selected")
            Return
        End If

        Dim directory = GetDirectory()
        If (not String.IsNullOrWhiteSpace(directory)) Then
            Dim result = SaveImagesToDisk(directory)
            if(result.ImageCount > 0)
                ShowMessage($"Saved {result.ImageCount} Image(s) to {directory}")
            Else 
                ShowMessage($"No image attachments found in selected email(s) messages")
            End If
        End If
    End sub
    
    ''' <summary>
    ''' Get an output directory
    ''' </summary>
    ''' <returns></returns>
    private Function GetDirectory() As String
        
        Dim dg = New FolderBrowserDialog()
        dg.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
        dg.ShowNewFolderButton = True
        Dim result = dg.ShowDialog()

        If (result = DialogResult.OK) Then
            Return dg.SelectedPath
        End If

        Return Nothing
    End Function
    
    ''' <summary>
    ''' Save the selected files to the disk
    ''' </summary>
    private Function SaveImagesToDisk(folder As String) As ExportResult
        dim result = new ExportResult()
        For Each item as MailItem In Me.Application.ActiveExplorer().Selection
            result.EmailCount += 1
            Dim attachments As Attachments = item.Attachments
            dim index = 0
            for each attach as Attachment in attachments 
                Dim fileName = attach.FileName
                Dim extension = UCase(Path.GetExtension(fileName))

                If ImageExtensions.Contains(extension) Then
                    index += 1
                    result.ImageCount += 1
                    Dim saveFileName = $"{item.SenderName}-{index}-{fileName}"
                    attach.SaveAsFile(Path.Combine(folder, saveFileName)) 
                End If
            Next
        Next
        Return result
    End Function

    ''' <summary>
    ''' Show a simple message dialog
    ''' </summary>
    ''' <param name="msg"></param>
    private Sub ShowMessage(msg As String)
        MessageBox.Show(msg)
    End Sub
End Class

Public Class ExportResult
    public Dim ImageCount as Integer
    public Dim EmailCount As Integer
End Class