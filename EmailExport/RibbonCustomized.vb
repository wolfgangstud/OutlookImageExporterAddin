﻿Imports Microsoft.Office.Interop.Outlook
Imports Microsoft.Office.Tools.Ribbon

Public Class RibbonCustomized

    Dim Exporter as ExportManager
    

    Private Sub RibbonCustomized_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load
        Exporter = new ExportManager(DirectCast(Me.Context, Explorer).Application)
    End Sub

    Private Sub btnExportImages_Click(sender As Object, e As RibbonControlEventArgs) Handles btnExportImages.Click
        Exporter.TrySaveImages()
    End Sub
End Class
