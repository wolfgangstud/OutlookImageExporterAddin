# Office Outlook Email Export Addin

A simply extensability tool for Outlook. Adds a custom button to the ribbon under a custom controls segment, when clicked it will prompt user for a folder location to save all the image attachments associated with the selected emails. Images are named based on sender and file name (i.e. John Doe-1-MyImage.png) for uniqueness.  

## Developers

- Paul Bradley
- Samuel Beecher
